import { Component, OnInit, Input } from '@angular/core';
import { Event } from '../../models/event.model';

@Component({
  selector: 'app-agenda-item',
  templateUrl: './agenda-item.component.html',
  styleUrls: ['./agenda-item.component.css']
})
export class AgendaItemComponent implements OnInit {
  @Input() eventItem: Event;

  constructor() { }

  ngOnInit(): void {
  }

}
