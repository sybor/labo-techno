import { Component, OnInit } from '@angular/core';
import { Event } from '../models/event.model';
import { EventService } from '../services/event.service';

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.css']
})
export class AgendaComponent implements OnInit {
  eventList: Event[];

  constructor(private eventService: EventService) { }

  ngOnInit(): void {
    this.eventList = this.eventService.getEvents();
  }

}
