import { Component, OnInit, Input } from '@angular/core';
import { Mantra } from 'src/app/models/mantra.model';

@Component({
  selector: 'app-mantra-item',
  templateUrl: './mantra-item.component.html',
  styleUrls: ['./mantra-item.component.css']
})
export class MantraItemComponent implements OnInit {
  @Input() mantraItem: Mantra;

  constructor() { }

  ngOnInit(): void {
  }

}
