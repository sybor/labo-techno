import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MantraItemComponent } from './mantra-item.component';

describe('MantraItemComponent', () => {
  let component: MantraItemComponent;
  let fixture: ComponentFixture<MantraItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MantraItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MantraItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
