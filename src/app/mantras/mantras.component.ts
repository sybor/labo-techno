import { Component, OnInit } from '@angular/core';
import { Mantra } from '../models/mantra.model';
import { MantraService } from '../services/mantra.service';

@Component({
  selector: 'app-mantras',
  templateUrl: './mantras.component.html',
  styleUrls: ['./mantras.component.css']
})
export class MantrasComponent implements OnInit {
  mantraList: Mantra[];

  constructor(private mantraService: MantraService) { }

  ngOnInit(): void {
    this.mantraList = this.mantraService.getMantras();
  }

}
