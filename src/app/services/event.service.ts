import { Injectable } from '@angular/core';
import { Event } from '../models/event.model';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  private events: Event[] = [

    new Event('Facebook live avec Sébastien et Gueules de loup en direct de la Cellule 133 à Bruxelles', 
    '21 juin 2020',
    'Regarder sur <a href="https://youtu.be/zGJ6gs2gEEA">Youtube</a>.'),

    new Event('Sortie du nouveau single d\'Astral Journey et Timour Montil', 
    '1 juin 2020',
    '<p>Ecouter la chanson sur <a href="https://music.apple.com/be/album/le-peuple-crie-feat-timour-montil-single/1515823864?l=fr">iTunes</a> \
    et <a href="https://open.spotify.com/track/30gvb4s5yszWr5XJhEMinB?si=i91iQ9POTiKpS2TKuy9pgA">Spotify</a>.'),

    new Event('Sortie de "Faith", le deuxième single d\'Astral Journey', 
    '6 avril 2020',
    '<p>Ecouter la chanson sur <a href="https://music.apple.com/be/album/faith/1506179264?i=1506179265&l=fr">iTunes</a> \
    et <a href="https://open.spotify.com/track/0Le6u965Mg27T43OMupoEC?si=nSkvO4zeT4CJkZI6Wl-feQ">Spotify</a>. \
   Regarder le clip sur <a href="https://youtu.be/OUSHTd2AjFg">Youtube</a>.</p>'),

    new Event('Sortie de "Excelsior", le premier single d\'Astral Journey', 
    '16 décembre 2019',
    '<p>Ecouter la musique sur <a href="https://music.apple.com/be/album/excelsior/1491189522?i=1491189524&l=fr">iTunes</a> \
    et <a href="https://open.spotify.com/track/4gjsHO7dGbq434uhFAJOwt">Spotify</a>. \
    Regarder le clip sur <a href="https://youtu.be/k6o1v4Hztek">Youtube</a>.'),

    new Event('Mise en ligne de "Seven", le premier clip live d\'Astral Journey', 
    '30 octobre, 2019',
    'Regarder le clip sur <a href="https://youtu.be/9dEQv8couHA">Youtube</a>.'),

    new Event('Sortie du clip de Romane Collin, "Blue lights"', 
    '9 octobre 2019',
    '<p>Regarder le clip sur <a href="https://youtu.be/9dEQv8couHA">Youtube</a>.'),

    new Event('Première chanson d\'Astral Journey disponible en ligne', 
    '15 septembre 2019',
    '<p>Ecouter la chanson sur <a href="https://www.youtube.com/watch?v=bvf5IhA5xmE">Youtube</a>.')
  ];

  getEvents() {
    return this.events;
  }

  constructor() { }
}
