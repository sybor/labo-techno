import { Injectable } from '@angular/core';
import { News } from '../models/news.model';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  private news: News[] = [

    new News('Sortie du nouveau single d\'Astral Journey !', 
    ' avec Timour Montil au chant', 
    '1 juin 2020',
    '../../assets/img/couverture-peuple.jpg',
    '<p>"Le peuple crie" est une collaboration avec Timour Montil du groupe belge de chanson française "Gueules de Loup". \
     La chanson est un message pour rester uni en cette période particulière.</p> \
     <p>Ecouter la chanson sur <a href="https://music.apple.com/be/album/le-peuple-crie-feat-timour-montil-single/1515823864?l=fr">iTunes</a> \
    et <a href="https://open.spotify.com/track/30gvb4s5yszWr5XJhEMinB?si=i91iQ9POTiKpS2TKuy9pgA">Spotify</a>.',
    'https://open.spotify.com/track/30gvb4s5yszWr5XJhEMinB?si=i91iQ9POTiKpS2TKuy9pgA'),

    new News('Sortie de Faith, le deuxième single d\'Astral Journey !', 
    'Un mantra pour retrouver son amour perdu', 
    '6 avril 2020',
    '../../assets/img/faith.jpg',
    '<p>Faith est un appel à l\'homme afin de se réconcilier avec sa joie et son âme. \
    La nature et les animaux sont un exemple de comment prendre soin les uns des autres.</p> \
    <p>Ecouter la chanson sur <a href="https://music.apple.com/be/album/faith/1506179264?i=1506179265&l=fr">iTunes</a> \
    et <a href="https://open.spotify.com/track/0Le6u965Mg27T43OMupoEC?si=nSkvO4zeT4CJkZI6Wl-feQ">Spotify</a>. \
   Regarder le clip sur <a href="https://youtu.be/OUSHTd2AjFg">Youtube</a>.</p>',
    'https://youtu.be/OUSHTd2AjFg'),

    new News('Sortie du premier single d\'Astral Journey !', 
    'Un lointain poème des légendes indiennes', 
    '16 décembre 2019',
    '../../assets/img/excelsior-cover.jpg',
    '<p>La chanson s\'inspire de la légende du roi Ravanna qui traversa  \
    toutes les rizières et montagnes, \
    depuis le Sri-Lanka jusqu\'au nord de l\'Inde.</p> \
    <p>Excelsior est un hymne d\'encouragement et d\'espoir pour tous les hommes et les femmes \
    afin de les guider sur leur chemin dans la grâce et la beauté.</p> \
    <p>Jata bhujan gapingala sphuratphanamaniprabha<br> \
    <i>May I find wonderful pleasure in Lord Shiva, who is the advocate of all life</i></p> \
    <p>Kadambakunkuma dravapralipta digvadhumukhe<br> \
    <i>With his creeping snake with its reddish brown hood and the shine of its gem on it</i></p> \
    <p>Madandha sindhu rasphuratvagutariyamedure<br> \
    <i>Spreading variegated colors on the beautiful faces of the Goddesses of the Directions</i></p> \
    <p>Mano vinodamadbhutam bibhartu bhutabhartari<br> \
    <i>Which is covered by a shimmering shawl made from the skin of a huge, inebriated elephant</i></p> \
    <p>Ecouter la musique sur <a href="https://music.apple.com/be/album/excelsior/1491189522?i=1491189524&l=fr">iTunes</a> \
    et <a href="https://open.spotify.com/track/4gjsHO7dGbq434uhFAJOwt">Spotify</a>.</p>',
    'https://youtu.be/k6o1v4Hztek'),

    new News('Nouveau studio Live "Seven"', 
    'Regarde notre premier studio Live', 
    '30 octobre, 2019',
    '../../assets/img/kirtan-seven.jpg',
    'Regarder le clip sur <a href="https://youtu.be/9dEQv8couHA">Youtube</a>.',
    'https://youtu.be/9dEQv8couHA'),

    new News('Clip avec Romane Collin', 
    'Blue Lights, une reprise de Jorja Smith', 
    '9 octobre 2019',
    '../../assets/img/romane-collin.jpg',
    'La chanteuse belge Romane Collin vient de sortir son dernier clip live. \
    Elle est accompagnée par Sébastien à la batterie et Kevin Jaucot aux claviers et à la production. \
    <p>Regarder le clip sur <a href="https://youtu.be/9dEQv8couHA">Youtube</a>.</p>',
    'https://www.youtube.com/watch?v=O_-x3UhZ08s'),

    new News('Première chanson avec Astral Journey', 
    'Remember: souviens-toi de ton infinité', 
    '15 septembre 2019',
    '../../assets/img/remember.png',
    'Rappelons-nous à quel point nous sommes puissants en tant qu\'êtres humains. \
    Depuis tout petit nous apprenons à nous conformer à la société \
    mais nous sommes nés avec un génie et une créativité illimitée. \
    Il est temps maintenant de s\'en souvenir...',
    'https://www.youtube.com/watch?v=bvf5IhA5xmE')
    
  ];

  getNews() {
    return this.news;
  }

  constructor() { }
}
