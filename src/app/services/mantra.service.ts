import { Injectable } from '@angular/core';
import { Mantra } from '../models/mantra.model';

@Injectable({
  providedIn: 'root'
})
export class MantraService {
  private mantras: Mantra[] = [

    new Mantra('ADES TISE ADES', 
    'Aad anil anaad anaahat, djoug djoug èko vès',
    'C\'est une salutation pour l\'infini, permettant de faire venir à soi la connaissance de l\'univers.'),

    new Mantra('ADI SHAKTI', 
    'Aadi shakti namo namo Sarabe shakti namo namo Pritham bhagvati namo namo Kundalini mata shakti namo namo',
    'Mantra dévotionnel pour invoquer la puissance créatrice primordiale. \
    C\'est une aide pour se libérer des insécurités qui entravent la liberté d\'agir'),

    new Mantra('GOBINDE MUKANDE', 
    'Gobindé Moukhandé Oudaré Aparé, Hariang Kariang, Nirnamé Akamé',
    'Elimine les blocages karmiques et les erreurs du passé, purifie le champ magnétique et favorise la relaxation.'),

    new Mantra('HAR HAR HAR HAR GOBINDE', 
    'Har Har Har Har Gobindé, Har Har Har Har Moukhandé, Har Har Har Har Oudaré, Har Har Har Har Aparé, \
    Har Har Har Har Hariang, Har Har Har Har Kariang, Har Har Har Har Nirnamé, Har Har Har Har Akamé',
    'Relie le mental à la prospérité et au pouvoir. Har, la force originelle de la création, est répété 4 fois afin de donner du pouvoir.\
    La peur est transformée en détermination et accroit la réserve d\'énergie.'),

    new Mantra('MOUL MANTRA', 
    'Ek Ong Kaar, Sat Nam, Kartaa Pourkh, Nirbhao, Nirvèr, Akâl Mourt, Adjouni, Sèbhang, Gour Presad, Djap ! \
    Aad Satch, Djougaad Satch, Hébi Satch, Naanak Hossi Bhi Satch',
    'Ce mantra est une boussole pour notre âme, \
    il forme la fondation de la conscience que nous voudrions développer et qui réside au plus profond de notre âme.'),

    new Mantra('RA MA DA SA', 
    'Ra Ma Da Sa Sa Sé So Hang',
    'Mantra de guérison, il harmonise le soi et apporte l\'équilibre. Les huits syllabes stimulennt l\'énergie de la colonne vertébrale \
    dans un but de guérison. Ce mantra combine la Terre (Ra Ma Da) et l\'éther (Sa Se So Hang)'),

    new Mantra('SAT SIRI AKAL', 
    'Sat Siri Siri Akal Siri Akal Maha Akal Maha Akal Sat Nam Akal Mourt Ouahé Gourou',
    'Ce mantra nous aide à établir notre être au-delà des changements du temps. Nous sommes éternels et tout ce que nous faisons\
    vient de notre âme.')
  ];

  getMantras() {
    return this.mantras;
  }

  constructor() { }
}

