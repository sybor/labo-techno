import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AboutComponent } from './about/about.component';
import { NewsComponent } from './news/news.component';
import { VideoComponent } from './video/video.component';
import { AudioComponent } from './audio/audio.component';
import { NewsItemComponent } from './news/news-item/news-item.component';
import { AgendaComponent } from './agenda/agenda.component';
import { MantrasComponent } from './mantras/mantras.component';
import { ContactComponent } from './contact/contact.component';
import { AgendaItemComponent } from './agenda/agenda-item/agenda-item.component';
import { MantraItemComponent } from './mantras/mantra-item/mantra-item.component';

const appRoutes: Routes = [
  {path: '', component: NewsComponent},
  {path: 'home', component: NewsComponent},
  {path: 'video', component: VideoComponent},
  {path: 'audio', component: AudioComponent},
  {path: 'agenda', component: AgendaComponent},
  {path: 'mantras', component: MantrasComponent},
  {path: 'contact', component: ContactComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    AboutComponent,
    NewsComponent,
    VideoComponent,
    AudioComponent,
    NewsItemComponent,
    AgendaComponent,
    MantrasComponent,
    ContactComponent,
    AgendaItemComponent,
    MantraItemComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
