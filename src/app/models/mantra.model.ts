export class Mantra {
    constructor(
        public title: string,
        public lyric: string,
        public comment: string
    ) {}
}