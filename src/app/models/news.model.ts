export class News {
    constructor(
        public title: string,
        public subtitle: string,
        public date: string,
        public imageUrl: string,
        public text: string,
        public link: string
    ) {}
}