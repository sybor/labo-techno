export class Event {
    constructor(
        public title: string,
        public date: string,
        public text: string
    ) {}
}